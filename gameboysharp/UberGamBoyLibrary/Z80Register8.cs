﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UberGamBoyLibrary
{

    /// <summary> Reprisents an 8-Bit Register </summary>
    class Z80Register8
    {
        /// <summary> Actual Register Data </summary>
        protected byte[] x8r;

        /**
         * <summary>
         * Initializes the Register with all 0 values.
         * </summary>
         * 
         */
        public Z80Register8()
        {
            x8r = new byte[8];
        }

        /**
         * <summary>
         * Initializes the Register with values from provided byte Array.
         * </summary>
         * <exception cref="NullReferenceException">Array is not 8 bytes</exception>
         */
        public Z80Register8(byte[] values)
        {
            if(values.Count<byte>()!=8)
                throw new NullReferenceException("Provided Non 8 Bit Register.");
            x8r = values;
        }

        /// <summary>
        /// Initializes the Register with values specified.
        /// </summary>
        /// <param name="a">A Register</param>
        /// <param name="b">B Register</param>
        /// <param name="c">C Register</param>
        /// <param name="d">D Register</param>
        /// <param name="e">E Register</param>
        /// <param name="h">H Register</param>
        /// <param name="l">L Register</param>
        /// <param name="f">F Register</param>
        /// <returns></returns>
        public Z80Register8(byte a = 0, byte b = 0, byte c = 0, byte d = 0, byte e = 0, byte h = 0, byte l = 0, byte f = 0)
        {
            x8r = new byte[8];
        }

        /**
         * <summary>
         * The indexes of the 8-Bit Registers.
         * </summary>
         */
        public enum x8Registers
        {
            a = 0,
            b = 1,
            c = 2,
            d = 3,
            e = 4,
            h = 5,
            l = 6,
            /** <summary> The flags register (F) is important to the functioning of the processor: it automatically calculates certain bits, or flags, based on the result of the last operation. </summary>*/
            f = 7
        }

    }
}
