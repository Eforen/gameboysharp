﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UberGamBoyLibrary
{
    public class Z80
    {

        /**
         * <summary>
         * Holds the m clock.
         * </summary>
         */
        protected int m;
        /**
         * <summary>
         * Holds the t clock.
         * </summary>
         */
        protected int t;

        /**********************************
         ** Registers                    **
         **********************************/
        Z80Register8 x8r = new Z80Register8();
        byte[] x16r = new byte[2];

        public Z80(){
            m = 0;
            t = 0;
        }


    }
}
