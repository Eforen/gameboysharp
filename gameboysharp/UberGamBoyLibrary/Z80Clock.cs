﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UberGamBoyLibrary
{
    /// <summary> Time clock: The Z80 holds two types of clock (m and t) </summary>
    class Z80Clock
    {
        /// <summary> M Clock value </summary>
        int m = 0;
        /// <summary> T Clock value </summary>
        int t = 0;

        public Z80Clock(int m = 0, int t = 0)
        {
            this.m = m;
            this.t = t;
        }

        /// <summary> Increment both timers by and amount. (Default: 1) </summary>
        public Z80Clock inc(int v = 1)
        {
            m+=v;
            t+=v;
            return this;
        }

        /// <summary> Decrement both timers by and amount. (Default: 1) </summary>
        public Z80Clock dec(int v = 1)
        {
            m-=v;
            t-=v;
            return this;
        }
    }
}
