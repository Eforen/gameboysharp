﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using net.sf.jni4net;
using System.Drawing.Drawing2D;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;

namespace gameboysharp
{
    public partial class MainForm : Form
    {
        private bool drag; // determine if we should be moving the form
        private Point startPoint = new Point(0, 0);
        public MainForm()
        {
            InitializeComponent();
            this.BackBoard.Focus();
            //setupTabs();
        }

        #region Window Managment Code

        void wnd_setup()
        {
            this.Resize += new EventHandler(size_Resize);
        }

        void Title_MouseUp(Object sender, MouseEventArgs e)
        {
            this.drag = false;
            //this.SubTitleBar.Focus();
            this.BackBoard.Focus();
        }

        void Title_MouseDown(Object sender, MouseEventArgs e)
        {
            this.startPoint = e.Location;
            this.drag = true;
        }

        void Title_MouseMove(Object sender, MouseEventArgs e)
        {
            if (this.drag)
            { // if we should be dragging it, we need to figure out some movement
                Point p1 = new Point(e.X, e.Y);
                Point p2 = this.PointToScreen(p1);
                Point p3 = new Point(p2.X - this.startPoint.X,
                    p2.Y - this.startPoint.Y);
                this.Location = p3;
            }
        }
        void CloseBtn_Click(Object sender, EventArgs e)
        {
            Close();
        }
        void RefreshBtn_Click(Object sender, EventArgs e)
        {
            //var setup = new BridgeSetup();
            //setup.Verbose = true;
            //foreach(string s in CLASSPATH){
            //    setup.AddClassPath(s);
            //}
            //Bridge.CreateJVM(setup);

        }

        private const int HTRIGHT = 11;
        private const int HTBOTTOM = 15;
        private const int HTBOTTOMRIGHT = 17;
        private const int WM_NCHITTEST = 0x84;

        private const int HTCAPTION = 0x2;
        private const int WM_NCLBUTTONDOWN = 0xA1;

        private Size SizeGripSize = new Size(15, 15); // <-- play with different sizes till it looks right to you

        private void statusStrip1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e){
            if (this.WindowState != FormWindowState.Maximized){
                if (e.Button == System.Windows.Forms.MouseButtons.Left){
                    this.Capture = false;
                    Cursor theCursor = Cursors.Arrow;
                    IntPtr Direction = new IntPtr(Bottom);
                    
                    if ((e.X <= statusStrip1.Width - 1 && e.X >= statusStrip1.Width - SizeGripSize.Width) || (e.Y <= statusStrip1.Height - 1 && e.Y >= statusStrip1.Height - SizeGripSize.Height))
                    {
                        if (e.X >= statusStrip1.Width - SizeGripSize.Width && e.X <= statusStrip1.Width - 1 && e.Y >= statusStrip1.Height - SizeGripSize.Height && e.Y <= statusStrip1.Height - 1)
                        {
                            Direction = (IntPtr)HTBOTTOMRIGHT;
                            theCursor = Cursors.SizeNWSE;
                        }
                        this.Cursor = theCursor;
                        Message msg = Message.Create(this.Handle, WM_NCLBUTTONDOWN, Direction, IntPtr.Zero);
                        base.WndProc(ref msg);
                    }
                }
            }
        }
        
        private void statusStrip1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e ){
                if (this.WindowState != FormWindowState.Maximized){
                    if ((e.X <= statusStrip1.Width - 1 && e.X >= statusStrip1.Width - SizeGripSize.Width) || (e.Y <= statusStrip1.Height - 1 && e.Y >= statusStrip1.Height - SizeGripSize.Height))
                    {
                        Cursor theCursor = Cursors.Arrow;
                        if (e.X >= statusStrip1.Width - SizeGripSize.Width && e.X <= statusStrip1.Width - 1 && e.Y >= statusStrip1.Height - SizeGripSize.Height && e.Y <= statusStrip1.Height - 1)
                        {
                            theCursor = Cursors.SizeNWSE;
                            if (e.Button == System.Windows.Forms.MouseButtons.Left){
                                //this.ARNG_WINDOW(this.Size);
                            }
                        }
                        this.Cursor = theCursor;
                    }
                }
        }

        private void statusStrip1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        void size_Resize(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void size_MouseDown(object sender, MouseEventArgs e)
        {
            OnMouseDown(e);
        }

        /*
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                this.Capture = false;
                Message msg = Message.Create(this.Handle, WM_NCLBUTTONDOWN, (IntPtr)HTCAPTION, IntPtr.Zero);
                base.WndProc(ref msg);
            }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCHITTEST:
                    Point loc = this.PointToClient(Cursor.Position);
                    bool blnRight = (loc.X > this.Width - this.SizeGripSize.Width);
                    bool blnBottom = (loc.Y > this.Height - this.SizeGripSize.Height);
                    if (blnRight && blnBottom)
                    {
                        m.Result = (IntPtr)HTBOTTOMRIGHT;
                        return;
                    }
                    else if (blnRight)
                    {
                        m.Result = (IntPtr)HTRIGHT;
                        return;
                    }
                    else if (blnBottom)
                    {
                        m.Result = (IntPtr)HTBOTTOM;
                        return;
                    }
                    break;
            }
            base.WndProc(ref m);
        }
        */

        #endregion Window Managment Code
        
        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
